import React, { Component } from "react";
import "./header.css";

class Header extends Component {
  state = {};

  render() {
    return (
      <nav className="navbar navbar-light navbar-color">
        <a className="navbar-brand">Contact List</a>
        <div className="input-group input-position">
          <input
            type="text"
            className="form-control border input-rounded border-right-0"
            placeholder="Search..."
          />
          <span className="input-group-append">
            <button
              className="border border-left-0 button-rounded"
              type="button"
            >
              <i className="material-icons search-icon-position">search</i>
            </button>
          </span>
        </div>
      </nav>
    );
  }
}

export default Header;
