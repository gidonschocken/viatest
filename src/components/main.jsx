import React, { Component } from "react";
import "../general.css";
import ContactList from "./contactList/contactList";
import Header from "./header/header";

class Main extends Component {
  state = {
    data: []
  };

  render() {
    if (this.state.data.length > 0) {
      return (
        <div>
          <div className="headerContainer">
            <Header />
          </div>
          <div className="contactListContainer">
            <ContactList list={this.state.data} />
          </div>
        </div>
      );
    } else {
      return <div />;
    }
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData = () => {
    fetch("http://private-05627-frontendnewhire.apiary-mock.com/contact_list")
      .then(response => response.json())
      .then(data => this.setState({ data }));
  };
}

export default Main;
