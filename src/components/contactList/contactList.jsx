import React, { Component } from "react";
import "./contactList.css";
import defaultImage from "../../images/default-image.png";
import driverIconCitizen from "../../images/citizen.svg";
import driverIconProf from "../../images/professional.svg";

class ContactList extends Component {
  state = {
    data: this.props.list,
    isMouseInside: false,
    isMouseIndex: -1
  };

  render() {
    const data = this.state.data;
    return (
      <div className="container mt-5">
        <div className="row">
          {data.map((item, index) => {
            return (
              <div
                className="col-lg-3 col-sm-4 col-xs-12 d-flex-mobile d-flex align-items-stretch"
                key={index}
              >
                <div
                  className="card flex-fill mb-4"
                  onMouseEnter={() => this.mouseEnter(index)}
                  onMouseOut={() => this.mouseLeave(index)}
                >
                  <img
                    className="card-img-top card-img-mobile p-2"
                    src={this.contactLoadImage(item.profile_image)}
                    onError={e => {
                      e.target.src = defaultImage;
                    }}
                    alt="driver pic"
                  />
                  <div className="card-body card-position">
                    <img
                      className="card-icon"
                      src={this.contactLoadIcon(item.driverType)}
                      alt="driver type"
                    />
                    <div className="card-title">{item.name}</div>
                    <div className="card-subtitle mb-2 text-muted">
                      Driver Rank - {item.driverRank} {this.state.isMouseInside}
                    </div>
                    {this.contactHover(item, index)}
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }

  contactHover = (item, index) => {
    let driverPhone = null;
    let driverEmail = null;
    if (item.phone)
      driverPhone = (
        <div className="card-subtitle card-hovered-text mb-1 text-muted">
          Phone Number: {item.phone}
        </div>
      );
    if (item.email)
      driverEmail = (
        <div className="card-subtitle card-hovered-text mb-1 text-muted">
          Email: {item.email}
        </div>
      );
    if (this.state.isMouseInside && this.state.isMouseIndex === index)
      return (
        <div>
          {driverPhone}
          {driverEmail}
        </div>
      );
  };

  contactLoadIcon = driverType => {
    if (driverType.toLowerCase() === "citizen") return driverIconCitizen;
    else return driverIconProf;
  };

  contactLoadImage = src => {
    if (src) return src;
    else return defaultImage;
  };

  mouseEnter = index => {
    this.setState({ isMouseIndex: index, isMouseInside: true });
  };
  mouseLeave = index => {
    this.setState({ isMouseIndex: index, isMouseInside: false });
  };
}

export default ContactList;
